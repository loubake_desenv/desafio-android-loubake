package com.loubake.desafioandroidconcretesolutions;

import android.app.Application;
import android.content.Context;

/**
 * Created by Gabriel Loubake on 26/05/2016.
 */
public class ConcreteSolutionsApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
