package com.loubake.desafioandroidconcretesolutions.conexao;

import com.android.volley.VolleyError;

/**
 * Created by Gabriel Loubake on 26/05/2016.
 */
public abstract class ConexaoConcreteSolutions {

    private static final String URL_CONEXAO = "https://api.github.com/";

    public abstract void iniciarConexao(ConexaoListener conexaoListener) throws Exception;

    protected abstract void trataResposta(Object response);

    protected String getUrlCompleta() {
        return getUrlBase() + getUrlSufixo();
    }

    protected String getUrlBase() {
        return URL_CONEXAO;
    }

    protected abstract String getUrlSufixo();

    protected abstract int getMethod();

    public interface ConexaoListener {
        void conexaoTerminouComSucesso(Object response);
        void conexaoTerminouComErro(VolleyError error);
    }
}
