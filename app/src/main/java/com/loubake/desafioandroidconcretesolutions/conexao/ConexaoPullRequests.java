package com.loubake.desafioandroidconcretesolutions.conexao;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.loubake.desafioandroidconcretesolutions.ConcreteSolutionsApplication;
import com.loubake.desafioandroidconcretesolutions.modelo.Autor;
import com.loubake.desafioandroidconcretesolutions.modelo.PullRequest;
import com.loubake.desafioandroidconcretesolutions.modelo.Repositorio;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gabriel Loubake on 26/05/2016.
 */
public class ConexaoPullRequests extends ConexaoConcreteSolutions {

    private static final String SUFIXO_URL_CONEXAO_REPOSITORIO = "repos/<criador>/<repositório>/pulls";

    protected ConexaoListener conexaoListener;
    private Repositorio repositorio;

    public ConexaoPullRequests(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public void iniciarConexao(final ConexaoListener conexaoListener) throws Exception {
        this.conexaoListener = conexaoListener;

        RequestQueue queue = VolleySingleton.getInstance(ConcreteSolutionsApplication.getContext()).
                getRequestQueue();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (getMethod(), getUrlCompleta(), null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        trataResposta(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        conexaoListener.conexaoTerminouComErro(error);
                    }
                });

        queue.add(jsonArrayRequest);
    }

    @Override
    protected void trataResposta(Object response) {
        List<PullRequest> pullRequests = new ArrayList<>();

        JSONArray resposta = (JSONArray)response;
        Gson gson = new Gson();

        try {
            for (int i=0; i<resposta.length(); i++) {
                String jsonObjectPullRequest = resposta.get(i).toString();
                PullRequest pullRequest = gson.fromJson(jsonObjectPullRequest, PullRequest.class);

                String jsonAutor = resposta.getJSONObject(i).getJSONObject("user").toString();
                Autor autor = gson.fromJson(jsonAutor, Autor.class);

                pullRequest.setAutor(autor);

                pullRequests.add(pullRequest);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        conexaoListener.conexaoTerminouComSucesso(pullRequests);
    }

    @Override
    public String getUrlSufixo() {
        String sufixo = SUFIXO_URL_CONEXAO_REPOSITORIO.replace("<criador>",repositorio.getAutor().getUsername());
        return sufixo.replace("<repositório>", repositorio.getNome());
    }

    @Override
    public int getMethod() {
        return Request.Method.GET;
    }
}
