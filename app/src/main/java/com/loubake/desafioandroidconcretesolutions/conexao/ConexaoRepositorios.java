package com.loubake.desafioandroidconcretesolutions.conexao;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.loubake.desafioandroidconcretesolutions.ConcreteSolutionsApplication;
import com.loubake.desafioandroidconcretesolutions.modelo.Autor;
import com.loubake.desafioandroidconcretesolutions.modelo.Repositorio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gabriel Loubake on 26/05/2016.
 */
public class ConexaoRepositorios extends ConexaoConcreteSolutions {

    private static final String SUFIXO_URL_CONEXAO_REPOSITORIO = "search/repositories";

    protected ConexaoListener conexaoListener;
    private int pageNumber;

    public ConexaoRepositorios(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    @Override
    public void iniciarConexao(final ConexaoListener conexaoListener) throws Exception {
        this.conexaoListener = conexaoListener;

        RequestQueue queue = VolleySingleton.getInstance(ConcreteSolutionsApplication.getContext()).
                getRequestQueue();
        JsonObjectRequest jsonObjRequest = new JsonObjectRequest
                (getMethod(), getUrlCompleta(), null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        trataResposta(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        conexaoListener.conexaoTerminouComErro(error);
                    }
                });

        queue.add(jsonObjRequest);
    }

    @Override
    protected void trataResposta(Object response) {
        List<Repositorio> repositorios = new ArrayList<>();

        JSONObject resposta = (JSONObject)response;
        Gson gson = new Gson();

        try {
            JSONArray arrayItems = resposta.getJSONArray("items");
            for (int i=0; i<arrayItems.length(); i++) {
                String jsonRepositorio = arrayItems.get(i).toString();
                Repositorio repositorio = gson.fromJson(jsonRepositorio, Repositorio.class);

                String jsonAutor = arrayItems.getJSONObject(i).getJSONObject("owner").toString();
                Autor autor = gson.fromJson(jsonAutor, Autor.class);

                repositorio.setAutor(autor);

                repositorios.add(repositorio);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        conexaoListener.conexaoTerminouComSucesso(repositorios);
    }

    @Override
    public String getUrlSufixo() {
        return SUFIXO_URL_CONEXAO_REPOSITORIO + getQueryString();
    }

    @Override
    public int getMethod() {
        return Request.Method.GET;
    }

    public String getQueryString() {
        return "?q=language:Java&sort=stars&page=" + pageNumber;
    }
}
