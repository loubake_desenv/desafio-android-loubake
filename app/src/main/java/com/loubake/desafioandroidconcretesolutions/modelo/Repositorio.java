package com.loubake.desafioandroidconcretesolutions.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class Repositorio implements Parcelable {
    @SerializedName("name") private String nome;

    @SerializedName("description") private String descricao;

    private Autor autor;

    @SerializedName("stargazers_count") private int stars;

    @SerializedName("forks") private int forks;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    // PARCELABLE

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(nome);
        out.writeString(descricao);
        out.writeParcelable(autor, flags);
        out.writeInt(stars);
        out.writeInt(forks);
    }

    public static final Parcelable.Creator<Repositorio> CREATOR
            = new Parcelable.Creator<Repositorio>() {
        public Repositorio createFromParcel(Parcel in) {
            return new Repositorio(in);
        }

        public Repositorio[] newArray(int size) {
            return new Repositorio[size];
        }
    };

    private Repositorio(Parcel in) {
        nome = in.readString();
        descricao = in.readString();
        autor = in.readParcelable(Autor.class.getClassLoader());
        stars = in.readInt();
        forks = in.readInt();
    }
}
