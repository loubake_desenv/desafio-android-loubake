package com.loubake.desafioandroidconcretesolutions.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class Autor implements Parcelable {

    @SerializedName("login") private String username;

    @SerializedName("avatar_url") private String urlFoto;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    // PARCELABLE

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(username);
        out.writeString(urlFoto);
    }

    public static final Parcelable.Creator<Autor> CREATOR
            = new Parcelable.Creator<Autor>() {
        public Autor createFromParcel(Parcel in) {
            return new Autor(in);
        }

        public Autor[] newArray(int size) {
            return new Autor[size];
        }
    };

    private Autor(Parcel in) {
        username = in.readString();
        urlFoto = in.readString();
    }
}
