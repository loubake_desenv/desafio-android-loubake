package com.loubake.desafioandroidconcretesolutions.modelo;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class PullRequest {
    @SerializedName("title") private String titulo;

    @SerializedName("body") private String body;

    private Autor autor;

    @SerializedName("created_at") private Date data;

    @SerializedName("html_url") private String url;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
