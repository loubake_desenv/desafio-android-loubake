package com.loubake.desafioandroidconcretesolutions.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Gabriel Loubake on 26/05/2016.
 */
public class DataUtils {

    public static String formatarData(Date data) {
        DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = targetFormat.format(data);
        return formattedDate;
    }
}
