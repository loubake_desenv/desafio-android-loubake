package com.loubake.desafioandroidconcretesolutions.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.loubake.desafioandroidconcretesolutions.R;
import com.loubake.desafioandroidconcretesolutions.adapter.PullRequestsAdapter;
import com.loubake.desafioandroidconcretesolutions.conexao.ConexaoConcreteSolutions;
import com.loubake.desafioandroidconcretesolutions.conexao.ConexaoPullRequests;
import com.loubake.desafioandroidconcretesolutions.modelo.PullRequest;
import com.loubake.desafioandroidconcretesolutions.modelo.Repositorio;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;

public class PullRequestsActivity extends AppCompatActivity {

    public static final String PARAMETRO_REPOSITORIO = "repositorio";

    private Repositorio repositorio;
    private ArrayList<PullRequest> pullRequests;
    private SuperRecyclerView pullRequestsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        Bundle params = getIntent().getExtras();
        if (params != null) {
            repositorio = params.getParcelable(PARAMETRO_REPOSITORIO);
        }

        pullRequestsRecycler = (SuperRecyclerView) findViewById(R.id.pull_requests_recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(repositorio.getNome());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pullRequests = new ArrayList<>();

        ConexaoPullRequests conexaoPullRequests = new ConexaoPullRequests(repositorio);
        try {
            conexaoPullRequests.iniciarConexao(new ConexaoConcreteSolutions.ConexaoListener() {
                @Override
                public void conexaoTerminouComSucesso(Object response) {
                    pullRequests = (ArrayList<PullRequest>)response;
                    configuraRecyclerView();
                }

                @Override
                public void conexaoTerminouComErro(VolleyError error) {
                    Toast.makeText(PullRequestsActivity.this, R.string.erro_inesperado, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void configuraRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        pullRequestsRecycler.setLayoutManager(linearLayoutManager);

        PullRequestsAdapter adapter = new PullRequestsAdapter(this,pullRequests);
        pullRequestsRecycler.setAdapter(adapter);
    }
}
