package com.loubake.desafioandroidconcretesolutions.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.loubake.desafioandroidconcretesolutions.R;
import com.loubake.desafioandroidconcretesolutions.adapter.RepositoriosAdapter;
import com.loubake.desafioandroidconcretesolutions.conexao.ConexaoConcreteSolutions;
import com.loubake.desafioandroidconcretesolutions.conexao.ConexaoRepositorios;
import com.loubake.desafioandroidconcretesolutions.modelo.Repositorio;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnMoreListener {

    private static final int PRIMEIRA_PAGINA = 1;
    private static final int NUMERO_ITENS_ANTES_CARREGAR_NOVAMENTE = 1;
    private static final int ERRO_AUTENTICACAO = 403;

    private static final String LISTA_ITENS = "repositorios";
    private static final String PAGINA_ATUAL = "pagina atual";

    private SuperRecyclerView repositoriosRecycler;
    private RepositoriosAdapter adapter;
    private ArrayList<Repositorio> repositorios;
    private int pageNumber = PRIMEIRA_PAGINA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        repositoriosRecycler = (SuperRecyclerView) findViewById(R.id.repositorios_recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            repositorios = new ArrayList<>();
            repositoriosRecycler.showProgress();
            iniciarConexao(pageNumber);
        }
        else {
            repositorios = (ArrayList<Repositorio>)savedInstanceState.getSerializable(LISTA_ITENS);
            pageNumber = savedInstanceState.getInt(PAGINA_ATUAL);

            configuraRecyclerView();
        }

        repositoriosRecycler.setupMoreListener(this, NUMERO_ITENS_ANTES_CARREGAR_NOVAMENTE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(LISTA_ITENS, repositorios);
        outState.putInt(PAGINA_ATUAL, pageNumber);
    }

    private void iniciarConexao(int numeroPagina) {
        ConexaoRepositorios conexaoRepositorios = new ConexaoRepositorios(numeroPagina);
        try {
            conexaoRepositorios.iniciarConexao(new ConexaoConcreteSolutions.ConexaoListener() {
                @Override
                public void conexaoTerminouComSucesso(Object response) {
                    configuraRecyclerView();

                    repositoriosRecycler.hideMoreProgress();

                    ((RepositoriosAdapter)repositoriosRecycler.getAdapter()).addAll((ArrayList<Repositorio>)response);

                    pageNumber++;
                }

                @Override
                public void conexaoTerminouComErro(VolleyError error) {
                    if (error.networkResponse.statusCode == ERRO_AUTENTICACAO) {
                        Toast.makeText(MainActivity.this, R.string.erro_autenticacao, Toast.LENGTH_SHORT).show();
                        repositoriosRecycler.hideMoreProgress();

                        repositoriosRecycler.getAdapter().notifyDataSetChanged();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configuraRecyclerView() {
        if (adapter == null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            repositoriosRecycler.setLayoutManager(linearLayoutManager);

            adapter = new RepositoriosAdapter(this,repositorios);
            repositoriosRecycler.setAdapter(adapter);
        }
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        iniciarConexao(pageNumber);
    }
}
