package com.loubake.desafioandroidconcretesolutions.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loubake.desafioandroidconcretesolutions.ConcreteSolutionsApplication;
import com.loubake.desafioandroidconcretesolutions.R;
import com.loubake.desafioandroidconcretesolutions.conexao.VolleySingleton;
import com.loubake.desafioandroidconcretesolutions.modelo.PullRequest;
import com.loubake.desafioandroidconcretesolutions.utils.DataUtils;
import com.loubake.desafioandroidconcretesolutions.viewHolder.PullRequestViewHolder;

import java.util.ArrayList;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<PullRequest> pullRequests;

    public PullRequestsAdapter(Context context, ArrayList<PullRequest> pullRequests) {
        this.context = context;
        this.pullRequests = pullRequests;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_requests_view_holder, parent, false);
        PullRequestViewHolder pullRequestViewHolder = new PullRequestViewHolder(view);
        return pullRequestViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((PullRequestViewHolder)holder).tituloPullRequest.setText(pullRequests.get(position).getTitulo());

        if (pullRequests.get(position).getBody() != null && !pullRequests.get(position).getBody().equals("")) {
            ((PullRequestViewHolder)holder).bodyPullRequest.setText(pullRequests.get(position).getBody());
        }
        else {
            ((PullRequestViewHolder) holder).bodyPullRequest.setVisibility(View.GONE);
        }

        ((PullRequestViewHolder)holder).usuarioNetworkImageView.setImageUrl(pullRequests.get(position).getAutor().getUrlFoto(),
                VolleySingleton.getInstance(ConcreteSolutionsApplication.getContext()).getImageLoader());
        ((PullRequestViewHolder)holder).usernameAutor.setText(pullRequests.get(position).getAutor().getUsername());
        ((PullRequestViewHolder)holder).dataPullRequest.setText(DataUtils.formatarData(pullRequests.get(position).getData()));

        ((PullRequestViewHolder)holder).layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlRequestPull = pullRequests.get(position).getUrl();
                if (!urlRequestPull.startsWith("http://") && !urlRequestPull.startsWith("https://")) {
                    urlRequestPull = "http://" + urlRequestPull;
                }
                Intent pullRequestBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(urlRequestPull));
                context.startActivity(pullRequestBrowser);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }
}
