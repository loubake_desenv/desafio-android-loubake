package com.loubake.desafioandroidconcretesolutions.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loubake.desafioandroidconcretesolutions.ConcreteSolutionsApplication;
import com.loubake.desafioandroidconcretesolutions.R;
import com.loubake.desafioandroidconcretesolutions.activity.PullRequestsActivity;
import com.loubake.desafioandroidconcretesolutions.conexao.VolleySingleton;
import com.loubake.desafioandroidconcretesolutions.modelo.Repositorio;
import com.loubake.desafioandroidconcretesolutions.viewHolder.RepositorioViewHolder;

import java.util.ArrayList;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class RepositoriosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Repositorio> repositorios;

    public RepositoriosAdapter(Context context, ArrayList<Repositorio> repositorios) {
        this.context = context;
        this.repositorios = repositorios;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repositorios_view_holder, parent, false);
        RepositorioViewHolder repoViewHolder = new RepositorioViewHolder(view);
        return repoViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((RepositorioViewHolder)holder).nomeRepositorio.setText(repositorios.get(position).getNome());

        if (repositorios.get(position).getDescricao() != null && !repositorios.get(position).getDescricao().equals("")) {
            ((RepositorioViewHolder) holder).descricaoRepositorio.setText(repositorios.get(position).getDescricao());
        }
        else {
            ((RepositorioViewHolder) holder).descricaoRepositorio.setVisibility(View.GONE);
        }

        ((RepositorioViewHolder)holder).forks.setText(ConcreteSolutionsApplication.getContext().getString(R.string.forks) + ": " +
                repositorios.get(position).getForks());
        ((RepositorioViewHolder)holder).stars.setText(ConcreteSolutionsApplication.getContext().getString(R.string.stars) + ": " +
                repositorios.get(position).getStars());
        ((RepositorioViewHolder)holder).usuarioNetworkImageView.setImageUrl(repositorios.get(position).getAutor().getUrlFoto(),
                VolleySingleton.getInstance(ConcreteSolutionsApplication.getContext()).getImageLoader());
        ((RepositorioViewHolder)holder).usernameAutor.setText(repositorios.get(position).getAutor().getUsername());

        ((RepositorioViewHolder)holder).layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent telaPullRequest = new Intent(context, PullRequestsActivity.class);

                Bundle params = new Bundle();
                params.putParcelable(PullRequestsActivity.PARAMETRO_REPOSITORIO, repositorios.get(position));
                telaPullRequest.putExtras(params);

                context.startActivity(telaPullRequest);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public void add(Repositorio repositorio) {
        insert(repositorio, repositorios.size());
    }

    public void insert(Repositorio repositorio, int position) {
        repositorios.add(position, repositorio);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        repositorios.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int size = repositorios.size();
        repositorios.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(ArrayList<Repositorio> listaRepositorios) {
        int startIndex = repositorios.size();
        repositorios.addAll(startIndex, listaRepositorios);
        notifyItemRangeInserted(startIndex, listaRepositorios.size());
    }
}
