package com.loubake.desafioandroidconcretesolutions.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.loubake.desafioandroidconcretesolutions.R;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class PullRequestViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout layout;
    public TextView tituloPullRequest;
    public TextView bodyPullRequest;
    public NetworkImageView usuarioNetworkImageView;
    public TextView usernameAutor;
    public TextView dataPullRequest;

    public PullRequestViewHolder(View view) {
        super(view);

        layout = (LinearLayout) view.findViewById(R.id.layout);
        tituloPullRequest = (TextView) view.findViewById(R.id.titulo_pull_request_text);
        bodyPullRequest = (TextView) view.findViewById(R.id.body_pull_request_text);
        usuarioNetworkImageView = (NetworkImageView) view.findViewById(R.id.usuario_network_image_view);
        usernameAutor = (TextView) view.findViewById(R.id.username_text);
        dataPullRequest = (TextView) view.findViewById(R.id.data_text);
    }
}