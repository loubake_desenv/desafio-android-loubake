package com.loubake.desafioandroidconcretesolutions.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.loubake.desafioandroidconcretesolutions.R;

/**
 * Created by Gabriel Loubake on 25/05/2016.
 */
public class RepositorioViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout layout;
    public TextView nomeRepositorio;
    public TextView descricaoRepositorio;
    public TextView forks;
    public TextView stars;
    public NetworkImageView usuarioNetworkImageView;
    public TextView usernameAutor;

    public RepositorioViewHolder(View view) {
        super(view);

        layout = (LinearLayout) view.findViewById(R.id.layout);
        nomeRepositorio = (TextView) view.findViewById(R.id.nome_repositorio_text);
        descricaoRepositorio = (TextView) view.findViewById(R.id.descricao_repositorio_text);
        forks = (TextView) view.findViewById(R.id.forks_repositorio_text);
        stars = (TextView) view.findViewById(R.id.stars_repositorio_text);
        usuarioNetworkImageView = (NetworkImageView) view.findViewById(R.id.usuario_network_image_view);
        usernameAutor = (TextView) view.findViewById(R.id.username_text);
    }
}